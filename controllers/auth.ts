import { Request, Response } from 'express';
import Brand from '../models/brand';
import Admin from '../models/admin';

export class AuthController {

    constructor() { }
    async login(req: Request, res: Response) {
        try {
            let admin = await Admin.findOne({email: req.body.email, password: req.body.password}).populate('brand')
            if(admin == null){
                let brand = await Brand.findOne({email: req.body.email, password: req.body.password})
                res.json({
                    token: "",
                    id: brand._id,
                    loginType: "OWNER",
                    isActivated: brand.isActive,
                    details: brand
                })
                return 
            }
            res.json({
                token: "",
                id: admin._id,
                loginType: "ADMIN",
                isActivated: admin.brand.isActive,
                details: admin

            })
            return

        } catch (error) {
            res.status(404).json({ message: "Not Found" })
        }

    }
}