import { Request, Response } from 'express';
import  Brand from '../models/brand';
import  Payment from '../models/payment';
import  {ValidateBrand} from '../helper/validator'
import {ICrud} from '../interfaces/ICrud'
import Branch from '../models/branch';
import { any } from 'joi';
import Package from '../models/package';


export class BrandController implements ICrud {

    constructor() { }
    async create(req: Request, res: Response) {
        try{
            req.body.loginType = 'OWNER'
            let validation = ValidateBrand(req.body)
            console.log(validation)

            if(validation.error){
                res.status(403).json({message: "Not Valid Data"})
                return
            }
            let brand  = await Brand.create(req.body)
            res.json({message: "The Brand is created successfully", id: brand._id})
        }catch(err){
            res.status(502).json({message: "Internal Server Error"})
        }
    }

    async update(req: Request, res: Response) {
        try{
            let validation = ValidateBrand(req.body)
            // if(validation.error != null){
            //     res.status(403).json({message: "Not Valid Data"})
            //     return
            // }
            let brand  = await Brand.findByIdAndUpdate(req.params.id,{
                $set: req.body
            })
            res.json({message: "The Brand is updated successfully", id: brand._id})
        }catch(err){
            res.status(402).json({message: "Not Valid Data"})
        }
    }
 
    async readAll(req: Request, res: Response) {
        try {
            let brands = await Brand.find({}).populate('branches').populate('admins')
            res.json(brands)
        } catch (error) {
            res.status(502).json({message: "Internal Server Error"})
        }

    }

    async read(req: Request, res: Response) {
        try {
            let brand = await Brand.findById(req.params['id']).populate('branches').populate({path: 'admins',populate: {
                path: 'adminRoles.branch',
                model: 'Branch'
            }})
            res.json(brand)
        } catch (error) {
            res.status(404).json({message: "Not Found"})
        }
        
    }

    async buyPackage (req:Request, res:Response){
        try {
            let paymentObj = {
                brand: req.params.id,
                status: "Compleeted",
                paymentMethod: "CASH",
                type: "POSPAID",
                name: "Package",
                price : 0,
                paymentId : "12345"

            }
            let {smsCount, emailCount, notificationCount}  = await Package.findById(req.body.packageID)
            let payment = await Payment.create(paymentObj)
            await Brand.findByIdAndUpdate(req.params.id,
            {
                $inc:{
                    smsCount : smsCount,
                    emailCount : emailCount,
                    notificationCount: notificationCount
                },
                $push:{
                    payments: payment._id
                }
            })
            res.json({message: "Done"})
        } catch (error) {
            res.status(404).json({message: "Not Found"})
        }
    }

    async getPayments(req:Request, res:Response){
        try {
            let brand = await Brand.findById(req.params.id).populate("payments")
            return res.json(brand)
        } catch (error) {
            res.status(404).json({message: "Not Found"})
        }
    }

    async renewPackages(req: Request, res:Response){
        let body = req.body
        let paymentObj =  {
            brand: req.params.id,
            status: "Compleeted",
            paymentMethod: "CASH",
            type: "PREPAID",
            name: "BRANCH",
            price : body.Price,
            paymentId : "12345",
            branches: body.branches

        }
        try {
            let payment = await Payment.create(paymentObj)
            await Brand.findByIdAndUpdate(req.params.id,
                {
                    $push:{
                        payments: payment._id
                    }
                })
                paymentObj.branches.forEach((branch: any) => {
                    Branch.findByIdAndUpdate(branch,{
                        $set: {startDate : new Date().toISOString(), isActive: true}
                    })
                }); 
                res.json({message: "Done"})
            
        } catch (error) {
            
        }
    }
}