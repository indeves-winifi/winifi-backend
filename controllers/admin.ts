import { Request, Response } from 'express';
import Brand from '../models/brand';
import { ValidateAdmin } from '../helper/validator'
import { ICrud } from '../interfaces/ICrud'
import Admin from '../models/admin';

export class AdminController implements ICrud {

    constructor() { }
    async create(req: Request, res: Response) {
        try {
            let validation = ValidateAdmin(req.body)
            console.log(validation)
            if (validation.error) {
                res.status(403).json({ message: "Not Valid Data" })
                return
            }
            let admin = await Admin.create(req.body)
            let result = await Brand.findByIdAndUpdate(admin.brand,
                {
                 $push:{
                     admins: admin._id
                 }
            })
            console.log(result)
            res.json({ message: "The Admin is created successfully", id: admin._id })
        } catch (err) {
            res.status(502).json({ message: "Internal Server Error" })
        }
    }

    async update(req: Request, res: Response) {
        try {
            // let validation = ValidateAdmin(req.body)
            // if (validation.error) {
            //     res.status(402).json({ message: "Not Valid Data" })
            // }
            let admin = await Admin.findByIdAndUpdate(req.params.id, {
                $set: req.body
            })
            res.json({ message: "The Admin is updated successfully", id: admin._id })
        } catch (err) {
            res.status(402).json({ message: "Not Valid Data" })
        }
    }

    async readAll(req: Request, res: Response) {

        throw new Error('Method not implemented.');


    }

    async read(req: Request, res: Response) {
        try {
            let admin = await Admin.findById(req.params['id']).populate('brand').populate('adminRoles.branch')
            res.json(admin)
        } catch (error) {
            res.status(404).json({ message: "Not Found" })
        }

    }

    async delete(req: Request, res: Response) {
        try {
            let admin = await Admin.findByIdAndDelete(req.params['id'])
            res.json(admin)
        } catch (error) {
            res.status(404).json({ message: "Not Found" })
        }

    }
}