"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminController = void 0;
const brand_1 = __importDefault(require("../models/brand"));
const validator_1 = require("../helper/validator");
const admin_1 = __importDefault(require("../models/admin"));
class AdminController {
    constructor() { }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let validation = validator_1.ValidateAdmin(req.body);
                console.log(validation);
                if (validation.error) {
                    res.status(403).json({ message: "Not Valid Data" });
                    return;
                }
                let admin = yield admin_1.default.create(req.body);
                let result = yield brand_1.default.findByIdAndUpdate(admin.brand, {
                    $push: {
                        admins: admin._id
                    }
                });
                console.log(result);
                res.json({ message: "The Admin is created successfully", id: admin._id });
            }
            catch (err) {
                res.status(502).json({ message: "Internal Server Error" });
            }
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // let validation = ValidateAdmin(req.body)
                // if (validation.error) {
                //     res.status(402).json({ message: "Not Valid Data" })
                // }
                let admin = yield admin_1.default.findByIdAndUpdate(req.params.id, {
                    $set: req.body
                });
                res.json({ message: "The Admin is updated successfully", id: admin._id });
            }
            catch (err) {
                res.status(402).json({ message: "Not Valid Data" });
            }
        });
    }
    readAll(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            throw new Error('Method not implemented.');
        });
    }
    read(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let admin = yield admin_1.default.findById(req.params['id']).populate('brand').populate('adminRoles.branch');
                res.json(admin);
            }
            catch (error) {
                res.status(404).json({ message: "Not Found" });
            }
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let admin = yield admin_1.default.findByIdAndDelete(req.params['id']);
                res.json(admin);
            }
            catch (error) {
                res.status(404).json({ message: "Not Found" });
            }
        });
    }
}
exports.AdminController = AdminController;
//# sourceMappingURL=admin.js.map