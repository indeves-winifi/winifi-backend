"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const brand_1 = __importDefault(require("../models/brand"));
const admin_1 = __importDefault(require("../models/admin"));
class AuthController {
    constructor() { }
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let admin = yield admin_1.default.findOne({ email: req.body.email, password: req.body.password }).populate('brand');
                if (admin == null) {
                    let brand = yield brand_1.default.findOne({ email: req.body.email, password: req.body.password });
                    res.json({
                        token: "",
                        id: brand._id,
                        loginType: "OWNER",
                        isActivated: brand.isActive,
                        details: brand
                    });
                    return;
                }
                res.json({
                    token: "",
                    id: admin._id,
                    loginType: "ADMIN",
                    isActivated: admin.brand.isActive,
                    details: admin
                });
                return;
            }
            catch (error) {
                res.status(404).json({ message: "Not Found" });
            }
        });
    }
}
exports.AuthController = AuthController;
//# sourceMappingURL=auth.js.map