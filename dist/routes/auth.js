"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const auth_1 = require("../controllers/auth");
const authController = new auth_1.AuthController();
router.post("/login", authController.login);
exports.default = router;
//# sourceMappingURL=auth.js.map