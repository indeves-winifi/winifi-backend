"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const brand_1 = __importDefault(require("./brand"));
const branch_1 = __importDefault(require("./branch"));
const admin_1 = __importDefault(require("./admin"));
const auth_1 = __importDefault(require("./auth"));
const package_1 = __importDefault(require("./package"));
class Router {
    constructor(server) {
        server.use('/brand', brand_1.default);
        server.use('/branch', branch_1.default);
        server.use('/admin', admin_1.default);
        server.use('/package', package_1.default);
        server.use('/auth', auth_1.default);
    }
}
exports.default = Router;
//# sourceMappingURL=index.js.map