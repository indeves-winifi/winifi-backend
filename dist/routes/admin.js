"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const admin_1 = require("../controllers/admin");
const adminController = new admin_1.AdminController();
router.post("/create", adminController.create);
router.put("/:id", adminController.update);
router.get("/:id", adminController.read);
router.delete("/:id", adminController.delete);
exports.default = router;
//# sourceMappingURL=admin.js.map