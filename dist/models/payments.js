"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const paymentSchema = new mongoose_1.Schema({
    brand: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: "Brand",
        required: true
    },
    name: {
        type: String,
    },
    type: {
        type: String,
    },
    price: {
        type: Number,
    },
    paymentMethod: {
        type: String,
    },
    paymentId: {
        type: String,
    },
    status: {
        type: String,
    },
    branches: [
        {
            type: mongoose_1.Schema.Types.ObjectId,
            ref: "Branch"
        }
    ]
});
const Payment = mongoose_1.model("Admin", paymentSchema);
exports.default = Payment;
//# sourceMappingURL=payments.js.map