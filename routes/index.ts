import * as express from 'express'
import brand from './brand'
import branch from './branch'
import admin from './admin'
import auth from './auth'
import Package from './package'



class Router {

    constructor(server: express.Express) {
        server.use('/brand', brand)
        server.use('/branch', branch)
        server.use('/admin', admin)
        server.use('/package', Package)
        server.use('/auth', auth)

    }
}

export default Router;