import { Router, Response } from "express";
const router: Router = Router();
import {AdminController} from "../controllers/admin"


const adminController = new AdminController();


router.post("/create",adminController.create);
router.put("/:id",adminController.update);
router.get("/:id",adminController.read);
router.delete("/:id",adminController.delete);

  

export default router