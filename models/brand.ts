import { Document, Model, model, Schema } from "mongoose";


export interface Ibrand extends Document {
    name: string;
    logo: string;
    email: string;
    password: string;
    address: string;
    contact: string;
    contactPerson: string
    regID: string;
    regImage: string;
    taxID: string;
    taxImage: string;
    smsApiKey: string;
    senderID: string;
    smtpIntegration: object;
    sendName: string
    smsCount: number
    emailCount: number
    notificationCount: number
    isActive: boolean
    isDeleted: boolean
    loginType: string;
    displayName: string;
    displayDesc: string;
    displayColor: string;

}

const brandSchema: Schema = new Schema({
    name: {
        type: String,
        required: true
    },
    loginType: {
        type: String,
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    logo: {
        type: String,
    },
    contact: {
        type: String,
    },
    contactPerson: {
        type: String,
    },
    address: {
        type: String,
    },
    regID: {
        type: String,
    },
    regImage: {
        type: String,
    },
    taxID: {
        type: String,
    },
    taxImage: {
        type: String,
    },
    smtpIntegration: {
        type: Object,
    },
    smsApiKey: {
        type: String,
    },
    senderID: {
        type: String,
    },
    sendName: {
        type: String,
    },
    displayColor: {
        type: String,
    },
    displayDesc: {
        type: String,
    },
    displayName: {
        type: String,
    },
    smsCount: {
        type: Number,
        default: 0

    },
    emailCount: {
        type: Number,
        default: 0

    },
    notificationCount: {
        type: Number,
        default: 0
    },
    isActive: {
        type: Boolean,
    },
    isDeleted: {
        type: Boolean,
    },
    branches:[
        {
            type: Schema.Types.ObjectId,
            ref: "Branch"
          }
    ],
    admins:[
        {
            type: Schema.Types.ObjectId,
            ref: "Admin"
          }
    ],
    payments:[
        {
            type: Schema.Types.ObjectId,
            ref: "Payment"
        }
    ],
    packageWallet:{
        smsCount:{
            type: Schema.Types.Number
        },
        emailCount:{
            type: Schema.Types.Number
        },
        notificationCount:{
            type: Schema.Types.Number
        }
    }
})

const Brand: Model<Ibrand> = model("Brand", brandSchema);

export default Brand;