import { Document, Model, model, Schema } from "mongoose";
import {Ibrand} from "./brand"

export interface Iadmin extends Document {
    brand: Ibrand['_id'];
    name: string;
    email: string;
    password: string;
    phone: string;
    isAllowedExport: boolean;
    loginType: string;
    adminRoles: object;
}

const adminSchema: Schema = new Schema({
    brand: {
        type: Schema.Types.ObjectId,
        ref: "Brand"
    },
    name: {
        type: String,
    },
    email: {
        type: String,
    },
    password: {
        type: String,
    },
    phone: {
        type: String,
    },
    loginType: {
        type: String,
    },
    adminRoles: [
         {
            branch: {
                type: Schema.Types.ObjectId,
                ref: "Branch"
            },
            allowedServices:{
                type: String,
            }
         }
    ]
})

const Admin: Model<Iadmin> = model("Admin", adminSchema);

export default Admin;