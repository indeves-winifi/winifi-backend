import { Document, Model, model, Schema } from "mongoose";
import { Ibrand } from "./brand"

export interface Ipayment extends Document {
    brand: Ibrand['_id'];
    name: string;
    type: string; // postpaid - prepaid
    price: number;
    paymentMethod: string; //cash - bank - online
    paymentId: string;
    status: string;
}

const paymentSchema: Schema = new Schema({
    brand: {
        type: Schema.Types.ObjectId,
        ref: "Brand",
        required: true
    },
    name: {
        type: String,
    },
    type: {
        type: String,
    },
    price: {
        type: Number,
    },
    paymentMethod: {
        type: String,
    },
    paymentId: {
        type: String,
    },
    status: {
        type: String,
    },
    branches: [
        {

            type: Schema.Types.ObjectId,
            ref: "Branch"
        }
    ]
})

const Payment: Model<Ipayment> = model("Payment", paymentSchema);

export default Payment;