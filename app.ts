import express from 'express'
import Router from './routes/index'
import * as bodyParser from 'body-parser'
import cors from 'cors'
import path from 'path'
class App {
  private httpServer: any

  constructor() {
    this.httpServer = express()
    this.httpServer.use(cors())  
    this.httpServer.use(bodyParser.urlencoded({ extended: true }));
    this.httpServer.use(bodyParser.json());
    this.httpServer.use("/customer",express.static(path.resolve(__dirname, '../customer')));
    this.httpServer.use("/winifi",express.static(path.resolve(__dirname, '../winifi')));
    this.httpServer.get('/winifi/*', function (req:express.Request, res:express.Response) {
      res.redirect("/winifi");
  });
  this.httpServer.get('/customer/*', function (req:express.Request, res:express.Response) {
    res.redirect("/customer");
  });
    new Router(this.httpServer);
  //   this.httpServer.get('*', function (req:express.Request, res:express.Response) {
  //     res.sendFile(path.resolve(__dirname , '../static/customer', 'index.html'));
  // });
  }

  public Start = (port: number) => {
    return new Promise((resolve, reject) => {

      this.httpServer.listen(
        port,
        () => {
          resolve(port)
        })
        .on('error', (err: object) => reject(err));
    })
  }
}

export default App;