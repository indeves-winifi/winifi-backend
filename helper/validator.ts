import Joi from 'joi'

const brandSchema = Joi.object({
    name: Joi.string().alphanum().min(3).max(30).required(),
    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    email: Joi.string().email({ minDomainSegments: 2 }),
    logo: Joi.string(),
    address: Joi.string(),
    contact: Joi.string(),
    contactPerson: Joi.string(),
    regID: Joi.string(),
    regImage: Joi.string(),
    taxID: Joi.string(),
    taxImage: Joi.string(),
    smsApiKey: Joi.string(),
    senderID: Joi.string(),
    smtpIntegration:  Joi.object(),
    sendName: Joi.string(),
    isActive: Joi.boolean(),
    isDeleted:  Joi.boolean(),
    loginType: Joi.string(),
    displayName: Joi.string(),
    displayDesc: Joi.string(),
    displayColor: Joi.string()

});

export  function ValidateBrand(brand: any){
    return brandSchema.validate(brand)
}


const adminSchema = Joi.object({
    name: Joi.string().alphanum().min(3).max(30).required(),
    password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    email: Joi.string().email({ minDomainSegments: 2 }),
    brand: Joi.string().required(),
    adminRoles: Joi.array()
});

export  function ValidateAdmin(admin: any){
    return adminSchema.validate(admin)
}

const packageScema = Joi.object({
    emailCount: Joi.number(),
    smsCount: Joi.number(),
    notificationCount: Joi.number(),
    name: Joi.string()
          .alphanum()
          .min(3)
          .max(30),
    expireTime: Joi.number(),
    currency: Joi.string(),
    price: Joi.number()
          .min(1)
  
  });
  
  export  function ValidatePackage(p: any){
    return packageScema.validate(p)
  }
  